//
//  ColuBrewTests.swift
//  ColuBrewTests
//
//  Created by Mr. Cohen on 21/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import XCTest
import Argo

@testable import ColuBrew

class ColuBrewTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBreweryApiIntegration() {
        Services.requestBreweries(target: .locations(locality:"miami", region:"Florida"), success: { json in
            XCTAssertNotNil(json, "json is nil")
            let breweryLocation: Decoded<BreweryLocations> = decode(json)
            XCTAssertNotNil(breweryLocation.value, "brewery location value could not be unwrapped")
            let bl = breweryLocation.value
            XCTAssertNotNil(bl?.data, "brewery data invalid")
        }, failure: { (error) in
            XCTAssert(false)
        })
    }

    
    func testBrewerySampleResultsJsonParsing() {
        let path = Bundle.main.path(forResource: "sampleBreweryResults", ofType: "json")
        XCTAssertNotNil(path, "bundle path returns no file")
        let data = NSData.init(contentsOfFile: path!)
        XCTAssertNotNil(data, "invalid json")
        let json = Services.parseJson(data: data as! Data)
        XCTAssertNotNil(json, "could not parse json")
        let breweryLocation: Decoded<BreweryLocations> = decode(json!)
        XCTAssertNotNil(breweryLocation.value, "brewery location value could not be unwrapped")
        let bl = breweryLocation.value
        XCTAssertNotNil(bl?.data, "brewery data invalid")
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
