//
//  ColuServicesTests.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 23/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import XCTest
import Argo

@testable import ColuBrew

class ColuGoogleTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGoogleMapsApiIntegration() {
        Services.requestGoogleMaps(target: .autocomplete(input: "miami"), success: { json in
            XCTAssertNotNil(json, "json is nil")
            let autocomplete: Decoded<Autocomplete> = decode(json)
            XCTAssertNotNil(autocomplete.value, "autocomplete value could not be unwrapped")
        }, failure: { (error) in
            XCTAssert(false)
        })
    }
    
    func testAutocompleteSampleJsonParsing() {
        let path = Bundle.main.path(forResource: "sampleGoogleAutocomplete", ofType: "json")
        XCTAssertNotNil(path, "bundle path returns no file")
        let data = NSData.init(contentsOfFile: path!)
        XCTAssertNotNil(data, "invalid json")
        let json = Services.parseJson(data: data as! Data)
        XCTAssertNotNil(json, "could not parse json")
        let autocomplete: Decoded<Autocomplete> = decode(json!)
        XCTAssertNotNil(autocomplete.value, "autocomplete value could not be unwrapped")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
