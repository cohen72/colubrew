//
//  AppDelegate.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 21/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//


import Foundation


public struct Constants {

	struct GoogleMapsAPI {
        
		static let BaseUrl = "https://maps.googleapis.com/maps/api"
        static let ApiKeyPlaces = "AIzaSyC99ICSfaIAjos8MtRZSEVJc-s5DjotBFs"
        static let ApiKeyMaps = "AIzaSyAJ5sEJ9y-BHPWcGieZqEZ4ytdMUhzGndw"
		
	}
    
    struct BreweryAPI {
        static let BaseUrl = "https://api.brewerydb.com/v2"
        static let ApiKey = "c82b0b05a350029db35380ed90d734da"
    }


}
