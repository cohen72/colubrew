//
//  Place.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 22/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct Autocomplete {
    let predictions: [Place]
}

extension Autocomplete: Decodable {
    public static func decode(_ json: JSON) -> Decoded<Autocomplete> {
        return curry(Autocomplete.init)
            <^> json <|| "predictions"
    }
}

struct Place {
    let id: String
    let place_id: String
    let description: String
    let main_text: String
    let secondary_text: String
}

extension Place: Decodable {
    public static func decode(_ json: JSON) -> Decoded<Place> {
        return curry(Place.init)
            <^> json <| "id"
            <*> json <| "place_id"
            <*> json <| "description"
            <*> json <| ["structured_formatting", "main_text"]
            <*> json <| ["structured_formatting", "secondary_text"]
        
    }
}



