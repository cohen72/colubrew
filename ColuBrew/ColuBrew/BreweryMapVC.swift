//
//  BreweryLocationsVC.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 22/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit
import GoogleMaps

class BreweryMapVC: UIViewController, GMSMapViewDelegate {

    var place: Place?
    var dataModel = BreweryMapDataModel()
    var mapView: GMSMapView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "locating breweries ..."
        getPlaceDetail()
    }
    
    override func loadView() {
        let camera = GMSCameraPosition.init()
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView!.isMyLocationEnabled = true
        mapView!.delegate = self
        view = mapView
    }
    
    func getPlaceDetail() {
        
        dataModel.getPlaceDetail(place: place, callback: { success in
            
            if (success) {
                if let placeDetail = self.dataModel.placeDetail {
                    let lat = CLLocationDegrees(placeDetail.lat)
                    let lng = CLLocationDegrees(placeDetail.lng)
                    let location = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 10.0)
                    self.mapView?.camera = location
                    self.getBreweries(placeDetail: placeDetail)
                }
            }
        })        
    }
    
    func getBreweries(placeDetail: PlaceDetail)  {
        self.dataModel.getBreweries(placeDetail: placeDetail, callback: { (success) in
            if (success) {
                self.createBreweryMarkers()
            }
            if let totalResults = self.dataModel.breweryLocations?.totalResults {
                self.title = "\(totalResults) Breweries"
            } else {
                self.title = "No Breweries"
            }
            
        })
    }
    
    
    func createBreweryMarkers() {
        if let breweryLocationsData = dataModel.breweryLocations?.data {
            for breweryData in breweryLocationsData {
                let marker = getBreweryMarker(breweryData: breweryData)
                marker.map = mapView
            }
        }
    }
    
    func getBreweryMarker(breweryData: BreweryData) -> GMSMarker {
        let marker = GMSMarker()
        let lat = CLLocationDegrees(breweryData.latitude!)
        let lng = CLLocationDegrees(breweryData.longitude!)
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        marker.title = breweryData.brewery?.name
        marker.snippet = breweryData.brewery?.website
        marker.userData = breweryData
        return marker
    }

//  MARK: Map View Delegate Methods
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        if let breweryView: BrewInfoView = Bundle.main.loadNibNamed("BrewInfoView", owner: self, options: nil)?.first as? BrewInfoView {
            let breweryData: BreweryData = marker.userData as! BreweryData
            breweryView.configure(breweryData: breweryData)
            return breweryView
        }
        return nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
