//
//  UIExtensions.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 23/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit

extension UIColor{    
    class func coluGreen() -> UIColor{
        return UIColor.init(red:103/255.0, green: 217/255, blue: 180/255, alpha: 1.0)
    }
}

extension UINavigationController {
    func customizeNavBar () {
        navigationBar.shadowImage = UIImage() //hides the slight gray line below the navigation bar
        navigationBar.barTintColor = UIColor.coluGreen()
        navigationBar.tintColor = UIColor.white
        navigationBar.backgroundColor = UIColor.coluGreen()
        let navigationTitleFont = UIFont(name: "Copperplate", size: 18)!
        navigationBar.titleTextAttributes = [NSFontAttributeName: navigationTitleFont, NSForegroundColorAttributeName: UIColor.white]
    }
}

extension UISearchController {
    func customizeSearchController () {
        hidesNavigationBarDuringPresentation = false
        dimsBackgroundDuringPresentation = false
        searchBar.placeholder = "Entery a city"
        searchBar.barTintColor = UIColor.coluGreen()
        searchBar.tintColor = UIColor.darkGray
    }
}
