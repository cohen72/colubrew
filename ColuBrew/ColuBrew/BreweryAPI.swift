//
//  BreweryAPI.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 22/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit
import Moya

public enum BreweryAPI {
    case locations(locality: String, region: String)
}

extension BreweryAPI: TargetType {
    public var baseURL: URL { return URL(string: Constants.BreweryAPI.BaseUrl)! }
    public var path: String {
        switch self {
        case .locations:
            return "/locations"
        }
    }
    public var method: Moya.Method {
        return .get
    }
    public var parameters: [String: Any]? {
        switch self {
        case .locations(let locality, let region):            
            return ["locality": locality.urlEscapedString, "region": region, "key": Constants.BreweryAPI.ApiKey]
        }
    }
    public var task: Task {
        return .request
    }
    public var validate: Bool {
        return true
    }
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }
}

