//
//  SearchDataModel.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 22/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit
import Argo

class CitySearchDataModel: NSObject {

    var places: [Place] = []
    
    func searchCities(input: String, callback: @escaping (Bool) -> Void) {
        
        Services.requestGoogleMaps(target: .autocomplete(input: input), success: { json in
            
            let autocomplete: Decoded<Autocomplete> = decode(json)
            guard let ac = autocomplete.value else {
                print("could not unwrap autocomplete value");
                callback(false)
                return
            }
            guard ac.predictions.count > 0 else {
                print("No predictions");
                callback(false)
                return
            }
            self.places = ac.predictions
            callback(true)
            
        }, failure: { (error) in
            print(error)
            callback(false)
        })
    }
    
    func clear() {
        self.places = []
    }
}
