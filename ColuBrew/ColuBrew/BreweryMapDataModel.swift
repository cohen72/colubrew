//
//  BreweryMapDataModel.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 22/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit
import Argo

class BreweryMapDataModel: NSObject {

    var placeDetail: PlaceDetail?
    var breweryLocations: BreweryLocations?
    
    func getPlaceDetail(place: Place?, callback: @escaping (Bool) -> Void) {
        
        if let place = place {
            Services.requestGoogleMaps(target: .placedetails(placeid: place.place_id), success: { json in
                let j = JSON(json)
                let placeDetail: Decoded<PlaceDetail> = j <| "result"
                guard let pd = placeDetail.value else {
                    print("could not unwrap place value");
                    callback(false)
                    return
                }
                self.placeDetail = pd
                callback(true)
                
            }, failure: { (error) in
                print(error)
                callback(false)
            })
        } else {
            callback(false)
            return
        }
    
    }
   
    func getBreweries(placeDetail: PlaceDetail?, callback: @escaping (Bool) -> Void) {
        
        if let placeDetail = placeDetail {
            
            Services.requestBreweries(target: .locations(locality: placeDetail.name, region: placeDetail.region), success:  { json in
                let locations: Decoded<BreweryLocations> = decode(json)
                guard let bl = locations.value else {
                    print("could not unwrap locations value");
                    callback(false)
                    return
                }
                self.breweryLocations = bl
                callback(true)
            }, failure: { (error) in
                print(error)
                callback(false)
            })
        } else {
            callback(false)
            return
        }
    }

}

