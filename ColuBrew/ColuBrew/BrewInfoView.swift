//
//  BrewInfoView.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 23/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class BrewInfoView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblWebsite: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
    func configure(breweryData: BreweryData) {
     
        self.lblName.text = breweryData.brewery?.name
        self.lblWebsite.text = breweryData.brewery?.website
        self.lblDescription.text = breweryData.brewery?.description
        self.lblPhone.text = breweryData.phone
        
        if let images = breweryData.brewery?.images {
            let URL = NSURL(string:images.icon)!
            self.imageView.af_setImage(withURL: URL as URL)
        }
        
    }
    
}
