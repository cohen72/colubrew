//
//  Brewery.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 22/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes


//MARK: Brewery Locations

struct BreweryLocations {
    let currentPage: Int
    let status: String
    let numberOfPages: Int?
    let totalResults: Int?
    let data: [BreweryData]?
}

extension BreweryLocations: Decodable {
    public static func decode(_ json: JSON) -> Decoded<BreweryLocations> {
        let locations = curry(BreweryLocations.init)
        return locations
            <^> json <| "currentPage"
            <*> json <| "status"
            <*> json <|? "numberOfPages"
            <*> json <|? "totalResults"
            <*> json <||? "data"
    }
}

//MARK: Brewery Data

struct BreweryData {
    let id: String
    let name: String
    let streetAddress: String?
    let extendedAddress: String?
    let locality: String?
    let region: String?
    let postalCode: String?
    let phone: String?
    let hoursOfOperation: String?
    let latitude: Float?
    let longitude: Float?
    let breweryId: String?
    let brewery: Brewery?
}

extension BreweryData: Decodable {
    public static func decode(_ json: JSON) -> Decoded<BreweryData> {
        let breweryData = curry(BreweryData.init)
            <^> json <| "id"
            <*> json <| "name"
            <*> json <|? "streetAddress"
            <*> json <|? "extendedAddress"
            <*> json <|? "locality"
            <*> json <|? "region"
        return breweryData
            <*> json <|? "postalCode"
            <*> json <|? "phone"
            <*> json <|? "hoursOfOperation"
            <*> json <|? "latitude"
            <*> json <|? "longitude"
            <*> json <|? "breweryId"
            <*> json <|? "brewery"
    }
}

//MARK: Brewery

struct Brewery {
    let id: String
    let name: String?
    let nameShortDisplay: String?
    let description: String?
    let website: String?
    let established: String?
    let isOrganic: String?
    let images: BreweryImages?
}

extension Brewery: Decodable {
    public static func decode(_ json: JSON) -> Decoded<Brewery> {
        let brewery = curry(Brewery.init)
            <^> json <| "id"
            <*> json <|? "name"
            <*> json <|? "nameShortDisplay"
            <*> json <|? "description"
        return brewery
            <*> json <|? "website"
            <*> json <|? "established"
            <*> json <|? "isOrganic"
            <*> json <|? "images"
    }
}

//MARK: Brewery Images

struct BreweryImages {
    let icon: String
    let medium: String
    let large: String
    let squareMedium: String
    let squareLarge: String
}

extension BreweryImages: Decodable {
    public static func decode(_ json: JSON) -> Decoded<BreweryImages> {
        return curry(BreweryImages.init)
        <^> json <| "icon"
        <*> json <| "medium"
        <*> json <| "large"
        <*> json <| "squareMedium"
        <*> json <| "squareLarge"
    }
}
