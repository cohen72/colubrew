//
//  ViewController.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 21/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//


import Foundation
import Moya
import Result

extension String {
    var urlEscapedString: String {
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: " ")
        allowed.addCharacters(in: "+")
        let encoded = self.addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
        return encoded!
    }
}

public func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}


private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}


struct Services {

    private static let googleMapsProvider = MoyaProvider<GoogleMapsAPI>()
    private static let breweryProvider = MoyaProvider<BreweryAPI>()
//    private static let breweryProvider = MoyaProvider<BreweryAPI>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])
    
    static func requestGoogleMaps(
        target: GoogleMapsAPI,
        success: @escaping (AnyObject!) -> Void,
        failure: @escaping (Moya.Error) -> Void)  {
            googleMapsProvider.request(target) { result in
            parseResult(result: result, success: success, failure: failure)
        }
    }
    
    static func requestBreweries(
        target: BreweryAPI,
        success: @escaping (AnyObject!) -> Void,
        failure: @escaping (Moya.Error) -> Void)  {
        breweryProvider.request(target) { result in
            parseResult(result: result, success: success, failure: failure)
        }
    }
    
    private static func parseResult(result: Result<Moya.Response, Moya.Error>, success: (AnyObject!) -> Void, failure: (Moya.Error) -> Void){

        switch result {
        case let .success(moyaResponse):
            
            if let parsedJson = parseJson(data: moyaResponse.data) {
                success(parsedJson)
            }
            
        case let .failure(error):
            print(error)
            break
        }
    }
    
    static func parseJson(data: Data) -> AnyObject? {
        let json: AnyObject? = try? JSONSerialization.jsonObject(with: data, options: []) as AnyObject
        if let j: AnyObject = json {
            return j
        }
        return nil
    }

}
