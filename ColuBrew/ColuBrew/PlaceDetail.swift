//
//  PlaceDetail.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 23/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes


struct PlaceDetail {
    let id: String
    let name: String
    let url: String
    let addressComponents: [AddressComponent]
    let lat: Float
    let lng: Float
    
}

extension PlaceDetail: Decodable {
    public static func decode(_ json: JSON) -> Decoded<PlaceDetail> {
        return curry(PlaceDetail.init)
            <^> json <| "id"
            <*> json <| "name"
            <*> json <| "url"
            <*> json <|| "address_components"
            <*> json <| ["geometry", "location", "lat"]
            <*> json <| ["geometry", "location", "lng"]
    }
    
    // Filter out the region (State in US) from the correct address component (with a 'administrative_area_level_1' type)
    
    var region: String {
        var region = ""
        let searchString = "administrative_area_level_1"
        let filteredArray = addressComponents.filter() { $0.types.contains(searchString) }
        if filteredArray.count > 0 {
            let addressComponent = filteredArray[0]
            region = addressComponent.longName
        }
        return region
    }
}

struct AddressComponent {
    let longName: String
    let shortName: String
    let types: [String]
    
    
}

extension AddressComponent: Decodable {
    public static func decode(_ json: JSON) -> Decoded<AddressComponent> {
        return curry(AddressComponent.init)
            <^> json <| "long_name"
            <*> json <| "short_name"
            <*> json <|| "types"
        
        
    }
}
