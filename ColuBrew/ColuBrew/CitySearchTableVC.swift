//
//  SearchTableViewController.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 21/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit


class CitySearchTableVC: UITableViewController, UISearchBarDelegate {

    let searchController = UISearchController(searchResultsController: nil)
    
    var dataModel = CitySearchDataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchResultsController()
        self.navigationController?.customizeNavBar()
    }
    func setupSearchResultsController() {
        searchController.customizeSearchController()
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        self.tableView.tableHeaderView = searchController.searchBar
    }
    
    func setupNavBar() {
        navigationController?.navigationBar.barTintColor = UIColor.coluGreen()
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.backgroundColor = UIColor.coluGreen()
        let navigationTitleFont = UIFont(name: "Copperplate", size: 18)!
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: navigationTitleFont, NSForegroundColorAttributeName: UIColor.white]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.places.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        let place: Place = dataModel.places[indexPath.row]
        cell.textLabel?.text = place.description

        return cell
    }
 
    //MARK: Search Delegate Methods

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            dataModel.clear()
            tableView.reloadData()
        }
    }
        
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dataModel.searchCities(input: searchBar.text!, callback: { success in
            self.tableView.reloadData()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if let identifier = segue.identifier {
            if identifier == "showMap" {
                let breweriesVc = segue.destination as! BreweryMapVC
                let indexPath = tableView.indexPathForSelectedRow
                let place: Place = self.dataModel.places[indexPath!.row]
                breweriesVc.place = place
                
                // customize back button title
                let backItem = UIBarButtonItem()
                backItem.title = " "
                navigationItem.backBarButtonItem = backItem
            }
        }
        
    }
    

}
