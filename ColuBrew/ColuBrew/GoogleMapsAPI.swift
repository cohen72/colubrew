//
//  ColuBrewAPI.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 21/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit
import Moya

public enum GoogleMapsAPI {
    case autocomplete(input: String)
    case placedetails(placeid: String)
}

extension GoogleMapsAPI: TargetType {
    public var baseURL: URL { return URL(string: Constants.GoogleMapsAPI.BaseUrl)! }
    public var path: String {
        switch self {
        case .autocomplete:
            return "/place/autocomplete/json"
        case .placedetails:
            return "/place/details/json"
        }
    }
    public var method: Moya.Method {
        return .get
    }
    public var parameters: [String: Any]? {
        switch self {
        case .autocomplete(let input):
            return ["types": "(cities)", "input": input, "key": Constants.GoogleMapsAPI.ApiKeyPlaces]
        case .placedetails(let placeid):
            return ["placeid": placeid, "key": Constants.GoogleMapsAPI.ApiKeyPlaces]
        }
    }
    public var task: Task {
        return .request
    }
    public var validate: Bool {
        return true
    }
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }
}
