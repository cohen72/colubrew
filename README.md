# Colu Brew #

Welcome to Colu Brew! Have you every wanted to find a brewery near you? Well now you can! Using Colu Brew Brewery Locator, simply type in a city, press "Search", and Colu Brew will take care of the rest. Once the results have been returned, press on the desired city to view the breweries within that location.

*Note: In future releases, using RxSwift, autocomplete predictions will show up as the city is being typed into the search bar field*



---

![coluBrewLaunch.png](https://bitbucket.org/repo/x4GGzG/images/1615377637-coluBrewLaunch.png)
![coluBrewSearch.png](https://bitbucket.org/repo/x4GGzG/images/2678384356-coluBrewSearch.png)
![coluBrewResults.png](https://bitbucket.org/repo/x4GGzG/images/3733611739-coluBrewResults.png)
![coluBrewMap.png](https://bitbucket.org/repo/x4GGzG/images/3294752409-coluBrewMap.png)
![coluBrewMarkerInfo.png](https://bitbucket.org/repo/x4GGzG/images/68435116-coluBrewMarkerInfo.png)

## Project Setup ##

In order to use Colu Brew, you'll need to a little setup ...

###1.  Google Maps API — [Get a Google Maps API Key](https://developers.google.com/places/web-service/get-api-key)
Google Maps API Key allows integration with the Google Maps API, the Google Maps SDK for iOS, driving the Google Map View section of the app.

###2. Google Places API — [Get a Google Maps API Key](https://developers.google.com/places/web-service/get-api-key)
Google Places API Key allows searching google's autocomplete suggestions for a city you type in to the search field.

###3. Brewery DB API — [Get a BreweryDB API Key](http://www.brewerydb.com/developers/apps)
Brewery DB API allows searching for all breweries according to a locality. In our case that locality is a city name.

###4. Add your API keys to the project
Navigate to the ```Constants.swift``` file and add all of your keys for each API.

## Installation ##

To install all the required libaries, navigate to the project home folder from terminal and type:

```
#!none
pod install

```


## Testing ##

ColuBrew includes a few automated unit and integration tests.  They test for the following:

* Successful communication with Google Maps API
* Successful communication with Google Maps API
* Successful communication with BreweryDB API
* Successful Parsing of a sample google autocomplete json 
* Successful Parsing of a real google autocomplete json 
* Successful Parsing of a sample brewery locations json 
* Successful Parsing of a real brewery locations json 

## Who do I talk to? ##

* If you have any questions about this repo, please contact Yehuda Cohen. He'll do what he can to make you feel warm and fuzzy inside.